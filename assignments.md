# JavaScript React Assignments
This assignment repository introduces functional and class based React Components and API network calls.  Students will understand the following:

- How data is passed to/from child/parent React Components
- The diferences between Functional and Class based Components
- How to make web API calls

When all the assignments have been completed, you will have a React application that will look similar to the screen shot below.  Every row in this application corresponds to one of the assignments.  The right column contains the React Components that you will be creating.

# BEWARE OF INFINITE LOOPS!!!
Make sure you don't get into an infinite loop with your `useEffect()` render functions!  Keep an eye on the Network tab in the browser Dev Tools and make sure you don't see a slurry of network calls.  

![](./docs/overview.png)

# Ex. 1  Counter
Create a React Component called `Counter` that is consumed by a parent Component in the following manner:

```html
<Counter count={this.state.counter}/>
```

This Component displays the value of count and displays the number when the counter increases by 10.  It displays an array of React logos (`/assets/react-logo.png`) every time the counter is incremented, and resets on every multiple of 10.

# Ex. 2 Tri-Color Progress Bar (Class Component)
Create a Class Component called `ProgressBarCC` that is consumed by a parent Component in the following manner:

```html
 <ProgressBarCC progress={this.state.progress1}/>
```

The value of `progess` is a number between 0-100, but the Component should correctly handle values over 100 and below 0.  The progress bar is red for values below 33%.  The progress bar is yellow between 34% and 66%.  The progress bar is green above 67%.  Note: Do not pass a percentage into this control (e.g. 30%, simply pass a number).  The 'Push me' button increments the progress bar by 10.

Bonus: Add a dark gray background to the progess bar.

# Ex. 3 Tri-Color Progress Bar (Functional Component)
Create a Class Component called `ProgressBarFC` that converts the `ProgressBarCC` Component in the previous excerise to a Functional Component.

```html
<ProgressBarFC progress={this.state.progress2}/>
```

# Ex. 4 Add & Delete Person
Create React Components (favor functional) called `AddPerson` and `PersonGrid` that are consumed by a parent Component in the following manner:

```html
<AddPerson onAddPerson={this.handleAddPerson}/>
<PersonGrid onRemovePerson={this.handleRemovePerson} people={this.state.personArray}/>
```

The `AddPerson` Component contains an HTML form that allows a user to add a person to the person array stored in the parent React Component.  The `PersonGrid` Component displays the list of people passed in as a property with a Delete button.

The Delete button deletes the person from the person grid and deletes the person from the person array.  NOTE: This is actually stated in reverse, deleting the person from the person array deletes the person from the grid.  React _reacts_ to state changes, this one-way data binding results in refreshes in the UI.

The `Push Me` button deletes the person array stored as a state variable in the parent Component.  This action deletes all the users displayed on the UI.

Bonus: Use the Tri-Color Progress Bar from the previous exercise to indicate the progress when a user enters the first name, last name, and age into the form fields.  When all three form fields have been entered the progress bar is green.

# Ex. 5 Get A Person from API
Create a React Component (favor functional) called `PersonInfo` that is consumed by a parent Component in the following manner:

```html
<PersonInfo personId={this.state.personId}/>
```

This Component makes the following API call:

```
GET https://jsonplaceholder.typicode.com/users/{personId}
```

When a user enters a valid person id in the textbox, the `PersonInfo` control makes a call to the API for the specified ID and displays the person id, name, and email.  If an invalid person id is entered, the Component should fail gracefully and display nothing on the UI.  Note, the jsonplaceholder API only contains 10 people, thus person id > 10 returns a 404.

**Keep an eye on the network tab in Dev Tools to make sure you are not recursively calling the API!**

# Ex. 6 List People & Get Tasks from API
Create React Components (favor functional) called `SelectPerson` and `TodoList` that are consumed by a parent Component in the following manner:

```html
<SelectPerson onChange={this.handleSelectChange}/>
<TodoList personId={this.state.selectedPersonId}/>
```

The `SelectPerson` makes the following API call to populate the drop down list.

```
GET https://jsonplaceholder.typicode.com/users/
```

The `TodoList` makes the following API call:

```
GET https://jsonplaceholder.typicode.com/todos?userId={personId}
```

When a user selects a person from the drop down list, the onChange event is fired and saves the person id to the parent Component's state.  This state variable is linked to the `personId` property of the `TodoList`.  The `TodoList` makes an API call for the `personId` passed into it.  The `TodoList` displays all the tasks for this person.

**Keep an eye on the network tab in Dev Tools to make sure you are not recursively calling the API!**
