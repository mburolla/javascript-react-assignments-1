import axios from "axios";

//
// GET: https://jsonplaceholder.typicode.com/todos?userId={personId}
//

export const getTodoListForPersonId = async (personId) => {
    try {
        let res = await axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${personId}`);
        return res.data;
    }
    catch(err) {
        console.log(err);
        return null;
    }
};

//
// GET: http://jsonplaceholder.typicode.com/users/{id}
//

export const getAllPeople = async (personId) => {
    try {
        let res = await axios.get(`https://jsonplaceholder.typicode.com/users/`);
        return res.data;
    }
    catch(err) {
        console.log(err);
        return null;
    }
};

//
// GET: http://jsonplaceholder.typicode.com/users/{id}
//

export const getPersonById = async (personId) => {
    try {
        let res = await axios.get(`https://jsonplaceholder.typicode.com/users/${personId}`);
        return res.data;
    }
    catch(err) {
        console.log(err);
        return null;
    }
};
