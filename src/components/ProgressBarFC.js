import React from 'react';
import { useState, useEffect } from 'react';
import '../css/ProgressBar.css';

const ProgressBarFC = (props) => {
    const [progress, setProgress] = useState('0');

    // Cap upper and lower boundaries.
    let theProgress = props.progress;
    if (props.progress < 0) {
        theProgress = 0;
    } else if (props.progress > 100) {
        theProgress = 100;
    }

    // Determine class name based on the progress.
    let className = "progressbar__step1";
    if (theProgress >= 33 && theProgress <= 66) {
        className = "progressbar__step2"
    } else if (theProgress > 66) {
        className = "progressbar__step3"
    }
    
    useEffect(() => {
        setProgress(theProgress + "%");
    }, [theProgress]); // Set the state based on props.

    return(
        <div className="progressbar">
            Progress: {props.progress}
            <div className="progressbar__outline">
                <div className={className}>
                    <div style={{width: progress}}>&nbsp;</div>
                </div>
            </div>
        </div>  
    );
}

export default ProgressBarFC;
