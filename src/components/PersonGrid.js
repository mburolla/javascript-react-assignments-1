import React from 'react';
import '../css/PersonGrid.css';

const PersonGrid = (props) => {
    return(
        <div className="persongrid">
            <table className="table">
                <tbody>
                {
                    props.people.map(p => 
                        <tr key={p.uuid}>
                            <td>
                                {p.firstName}
                            </td>
                            <td>
                                {p.lastName}
                            </td>
                            <td>
                                {p.age}
                            </td>
                            <td>
                                <button className="btn btn-danger" onClick={() => props.onRemovePerson(p)}>Delete</button>
                            </td>
                        </tr>)
                }
                </tbody>
            </table>
        </div>
    );   
}

export default PersonGrid;
