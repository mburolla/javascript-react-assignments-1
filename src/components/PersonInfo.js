import React from 'react';
import { useState, useEffect } from 'react';
import { getPersonById } from '../proxies/apiProxy';
import '../css/PersonInfo.css';

const PersonInfo = (props) => {
    let [person, setPerson] = useState(null);
    
    useEffect(() => {
        const fetchPerson = async () => { 
            setPerson(await getPersonById(props.personId)); 
        }
        fetchPerson();
    }, [props.personId]); 

    return(
        <div className="personinfo">
            Id: {person && person.id}<br />
            Name: {person && person.name}<br />
            Email: {person && person.email}
        </div>
    );       
}

export default PersonInfo;