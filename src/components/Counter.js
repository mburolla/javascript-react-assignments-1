import React from 'react';
import { useState, useEffect } from 'react';
import '../css/Counter.css';

const Counter = (props) => {
    const [tensCount, setTensCount] = useState(0);
    const [imgArray, setImgArray] = useState([]);

    // Don't build the array here, it will get built on 
    // every render for every component.

    useEffect(() => {
        // Build our image array.
        let tempArray = [];
        for (let i = 0; i < props.count % 10; i++) {
            tempArray.push(i);
        }
        if (props.count > 0 && props.count % 10 === 0) {
            for (let i = 0; i < 10; i++) {
                tempArray.push(i);
            }
        }
        setImgArray(tempArray);

        // Set tens count.
        if (props.count % 10 === 0) {
            setTensCount(props.count);
        }
    },[props.count]); 

    return(
        <div className="counter">
            <table className="counter__table">
                <tbody>
                    <tr>
                        <td id="counter__col1">
                            Counter: { props.count } <br />
                            Tens: { tensCount }
                        </td>
                        <td id="counter__col2">       
                            {
                                imgArray.map(x =>
                                    <img className="counter__image" key={x} src={require('../assets/react-logo.png')} alt="" width="45"/>
                                )    
                            }
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );   
}

export default Counter;
