import React from 'react';
import Counter from './Counter';
import TodoList from './TodoList';
import AddPerson from './AddPerson';
import PersonInfo from './PersonInfo';
import PersonGrid from './PersonGrid';
import ProgressBarCC from './ProgressBarCC';
import ProgressBarFC from './ProgressBarFC';
import SelectPerson from './SelectPerson';

const pkg = require('../../package.json');

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            counter: 0,         // Excerise 1
            progress1: 0,       // Excerise 2
            progress2: 0,       // Excerise 3
            personArray: [],    // Excerise 4
            personId: 1,        // Excerise 5
            selectedPersonId: 0 // Excerise 6
        };

        console.log(pkg.version);
    }

    render() {
        return (
            <div className="app">
                <div className="container-md">
                    <div className="row">
                        <div className="col-sm-3"> 
                            <button className="btn btn-secondary" onClick={this.handleClick}>Push me</button>
                        </div>
                        <div className="col-sm-9">
                          <Counter count={this.state.counter}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3"> 
                            <button className="btn btn-secondary" onClick={this.handleClick2}>Push me</button>
                        </div>
                        <div className="col-sm-9">
                           <ProgressBarCC progress={this.state.progress1}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3"> 
                            <button className="btn btn-secondary" onClick={this.handleClick3}>Push me</button>
                        </div>
                        <div className="col-sm-9">
                           <ProgressBarFC progress={this.state.progress2}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3"> 
                            <button className="btn btn-secondary" onClick={this.handleClick4}>Push me</button>
                        </div>
                        <div className="col-sm-9">
                            <AddPerson onAddPerson={this.handleAddPerson}/>
                            <PersonGrid onRemovePerson={this.handleRemovePerson} people={this.state.personArray}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3"> 
                            <br />
                            <input className="form-control" placeholder="Person Id (1-10)" onChange={(e) => this.handlePersonIdChange(e)}></input>
                        </div>
                        <div className="col-sm-9">
                            <PersonInfo personId={this.state.personId}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-9">
                            <SelectPerson onChange={this.handleSelectChange}/>
                            <br />
                            <TodoList personId={this.state.selectedPersonId}/>
                        </div>
                    </div>
                </div>  
            </div>
        );
    }

    //
    // Callbacks
    //

    handleRemovePerson = (person) => {
        let newPersonList = this.state.personArray.filter(p => p.uuid !== person.uuid);
        this.setState({personArray: newPersonList});
    }

    handleAddPerson = (data) => {
        this.state.personArray.push({
            "firstName": data.firstName,
            "lastName" : data.lastName,
            "age": data.age,
            "uuid": data.uuid
        })
        this.setState({personArray: this.state.personArray});
    }

    handleSelectChange = (personId) => {
        this.setState({selectedPersonId: personId});
    }

    //
    // Handlers
    //

    handleClick = () => {
        this.setState({counter: this.state.counter + 1});
    }

    handleClick2 = () => {
        this.setState({progress1: this.state.progress1 + 10});
    }

    handleClick3 = () => {
        this.setState({progress2: this.state.progress2 + 10});
    }

    handleClick4 = () => {
        this.setState({personArray: []});
    }

    handlePersonIdChange = (e) => {
        let personId = e.target.value;
        if (!isNaN(personId)) {
            this.setState({personId: personId});
        }
    }
}
