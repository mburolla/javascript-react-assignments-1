import React from 'react';
import '../css/ProgressBar.css';

export default class ProgressBarCC extends React.Component {
 
    constructor(props) {
        super(props);
        this.state = {
            progressPercent: 0,
            progressBarClassName: ""
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        // Cap upper and lower boundaries.
        let progressScaler = nextProps.progress;
        if (progressScaler > 100) {
            progressScaler = 100;
        }
        if (progressScaler < 0) {
            progressScaler = 0;
        }

        // Determine class name based on the progress.
        let className = "";
        if (progressScaler < 33) {
            className = "progressbar__step1"
        } else if (progressScaler >= 33 && progressScaler <= 66) {
            className = "progressbar__step2"
        } else if (progressScaler > 66) {
            className = "progressbar__step3"
        }

        // Set our state variables.
        return {
            progressPercent: progressScaler + '%',
            progressBarClassName: className
        };
    }

    render() {
        return (
            <div className="progressbar">
                Progress: {this.props.progress}
                <div className="progressbar__outline">
                    <div className={this.state.progressBarClassName}>
                        <div style={{width: this.state.progressPercent}}>&nbsp;</div>
                    </div>
                </div>
            </div>
          ); 
    }
}
