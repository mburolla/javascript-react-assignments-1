import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useForm } from 'react-hook-form';
import '../css/AddPerson.css';

const AddPerson = (props) => {
    
    const { register, handleSubmit, formState: { errors } } = useForm();
    
    const onSubmit = (data) => {
        data.uuid = uuidv4();
        props.onAddPerson(data); // Callback to parent.
    }
    
    return (
        <form className="addperson" onSubmit={handleSubmit(onSubmit)}>
            <input className="form-control" {...register('firstName')} placeholder="First Name"/> {/* register an input */}
            <input className="form-control" {...register('lastName', { required: true })} placeholder="Last Name" />
            {errors.lastName && <p>Last name is required.</p>}
            <input className="form-control" {...register('age', { pattern: /\d+/ })} placeholder="Age"/>
            {errors.age && <p>Please enter number for age.</p>}
            <input className="btn btn-secondary" type="submit" />
        </form>
    );
}

export default AddPerson;
