import React from 'react';
import { useState, useEffect } from 'react';
import { getTodoListForPersonId } from '../proxies/apiProxy';
import '../css/TodoList.css';

const TodoList = (props) => {
    let [todoList, setTodoList] = useState([]);

    useEffect(() => {
        const fetchPerson = async () => { 
            setTodoList(await getTodoListForPersonId(props.personId)); 
        }
        fetchPerson();
    }, [props.personId]); 

    if (props.personId ===0) {
        return null;
    }

    return (
        <div>
            <h3>TODO List</h3>
            <div className="todolist" style={{ height: '200px', overflow:'auto'}}>
                <table className="todolist__table">
                    <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Task</th>
                    </tr>
                        {
                            todoList.map(i => 
                            <tr key={i.id}>
                                <td>
                                    {i.id}
                                </td>
                                <td>
                                    {i.title}
                                </td>
                            </tr>) 
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default TodoList;
