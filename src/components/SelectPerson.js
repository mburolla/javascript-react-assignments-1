import React from 'react';
import { useState, useEffect } from 'react';
import { getAllPeople } from '../proxies/apiProxy';
import '../css/SelectPerson.css';

const SelectComponent = (props) => {
    let [personList, setPersonList] = useState([]);
    
    useEffect(() => {
        const fetchPerson = async () => { 
            setPersonList(await getAllPeople()); 
        }
        fetchPerson();
    }, []); 

    return(
        <div className="selectperson">
            <div>
                <select className="form-select" aria-label="Default select example" onChange={e => props.onChange(e.target.value)}>
                    {
                        personList.map(p => 
                            <option key={p.id} value={p.id}>{p.name}</option>
                        )
                    }
                </select>
            </div>
        </div>
    );
}

// const handleChange = (e) => {
//     console.log(e)
// }

export default SelectComponent;
