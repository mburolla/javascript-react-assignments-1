# JavaScript React Assignments
Assignments are located [here](./assignments.md).

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`

# BEWARE OF INFINITE LOOPS!!!
Make sure you don't get into an infinite loop with your `useEffect()` render functions!  Keep an eye on the Network tab in the browser Dev Tools and make sure you don't see a slurry of network calls.  

